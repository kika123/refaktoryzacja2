package pl.edu.pwsztar.domain.enums;

public enum FigureType {
    KING,
    QUEEN,
    ROOK,
    BISHOP,
    KNIGHT,
    PAWN
}
