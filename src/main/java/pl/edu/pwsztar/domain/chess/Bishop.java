package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.awt.Point;

@Component
@Qualifier("Bishop")
public class Bishop implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point start, Point end) {

        if(start == end) {
            return false;
        }

        int disX = Math.abs(start.x - end.x);
        int disY = Math.abs(start.y - end.y);

        if(disX == disY){
            return true;
        }

        return false;
    }
}