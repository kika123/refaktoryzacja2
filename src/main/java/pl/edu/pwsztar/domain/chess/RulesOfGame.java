package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.awt.Point;

public interface RulesOfGame {

    boolean isCorrectMove(Point start, Point end);
}
